class TextProcessing::StopWordsReader
  class << self
    def load(file_name="#{Rails.root}/data/stopwords.txt")
      words = []
      File.open(file_name, 'r:UTF-8') do |f|
        while not f.eof?
          line = f.readline.chomp.strip
          next if line.start_with?('|')
          line.split.each {|w| words << w}
        end
      end
      words
    end
  end
end
