class TextProcessing::SearchEngine
  attr_reader :search_time
  def initialize
    @search_time = nil
  end

  def search(user_text)
    start_time = Time.now
    stem_words = user_input_to_stem_array(user_text)
    main_index = TextProcessing::WordTokenizer.load(nil, 'index.main')

    finds_in_main = find_in_main_index(main_index, stem_words)
    #binding.pry
    # ранжировать документы
    # первыми идут, те у которых найдено максимальное кол-во разных слов, далее по кол-ву одних слов
    finds_files = convert_find_index(finds_in_main)
    #binding.pry
    sort_files = sort_find_files(finds_files)
    #binding.pry
    docs = to_doc(sort_files)
    @search_time = Time.now - start_time
    docs
  end

  def search_position(user_text, doc_id)
    stem_words = user_input_to_stem_array(user_text)
    doc_index = TextProcessing::WordTokenizer.load(nil, doc_id)
    finds = doc_index.select { |index| stem_words.include?(index[0]) }
    finds
  end

  private
  def user_input_to_stem_array(user_text)
    stem_words = []
    user_text.scan(/[а-яё]+/i) do |word|
      w = word.downcase.gsub("ё", "е")
      stem_words << TextProcessing::PorterStemmer.stem(w)
    end
    stem_words
  end

  def find_in_main_index(main_index, stem_words)
    finds_in_main = {}
    stem_words.each do |stem_word|
      finds_in_main[stem_word] = main_index[stem_word] || []
    end
    finds_in_main
  end

  def convert_find_index(finds_in_main)
    files = Hash.new { |hash, key| hash[key] = [] }

    finds_in_main.keys.each do |stem_word_key|
      finds_in_main[stem_word_key].each do |file_name, count|
        files[file_name] << [stem_word_key, count]
      end
    end
    files
  end

  def sort_find_files(files)
    files_array = []
    files.keys.each do |key|
      current_array = [key] + files[key] # merge arrays
      files_array << current_array
    end
    files_array.sort! do |x, y|
      if x.count == y.count
        x_sum = x[1..-1].reduce(0) { |prev, arr| prev + arr[1] }
        y_sum = y[1..-1].reduce(0) { |prev, arr| prev + arr[1] }
        y_sum <=> x_sum
      else
        y.count <=> x.count
      end
    end
  end

  def to_doc(files)
    docs = files.map { |file| Doc.new(id: "#{file[0]}.txt") }
                .select { |doc| doc.exists? }
  end
end

# s = TextProcessing::SearchEngine.new
# s.search('работа на дому')
# s = TextProcessing::SearchEngine.new; s.search('работа на дому')