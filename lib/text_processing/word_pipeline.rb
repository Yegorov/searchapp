class TextProcessing::WordPipeline
  def initialize(*procs)
    @procs = procs
  end
  def pipeline(dict)
    @procs.each do |proc|
      dict = proc.call(dict)
    end
    dict
  end
end
