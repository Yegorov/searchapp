require 'yaml'
require 'zlib'
require 'trie'

class TextProcessing::WordTokenizer
  attr_accessor :file_name, :dict

  FOLDER = "#{Rails.root}/data/indexes"

  def initialize(file_name)
    @file_name = file_name
  end

  def tokenize(with_regex: '[а-яё]+', &block)
    @dict ||= Hash.new { |hash, key| hash[key] = [] }
    doc = Doc.find(id: @file_name)
    doc.read
    doc.content.scan(/#{with_regex}/i) do |word|
      w = self.class.word_processing(word, &block)
      dict[w] << $~.begin(0)
    end
    nil
  end

  def dump(file_name=nil)
    return if @dict.nil?
    file_name ||= "#{@file_name.sub('.txt', '')}.words"
    File.open("#{FOLDER}/#{file_name}", 'w:UTF-8') do |f|
      f.write(YAML.dump(@dict))
    end
  end

  def self.dump(sub_folder=nil, file_name=nil, dict)
    current_file = "#{FOLDER}/#{file_name}"
    current_file = "#{FOLDER}/#{sub_folder}/#{file_name}" unless sub_folder
    File.open(current_file, 'wb:ASCII-8BIT') do |f| # 'w:UTF-8'
      f.write(Zlib::Deflate.deflate(YAML.dump(dict)))
    end
  end
  def self.load(sub_folder=nil, file_name)
    current_file = "#{FOLDER}/#{file_name}"
    current_file = "#{FOLDER}/#{sub_folder}/#{file_name}" unless sub_folder
    File.open(current_file, 'rb:ASCII-8BIT') do |f| # 'r:UTF-8'
      # binding.pry
      YAML.load(Zlib::Inflate.inflate(f.read))
    end
  end
  def self.hash_to_trie(h)
    t = Trie.new
    h.each do |k, v|
      t.add(k, v)
    end
    t
  end
  # t.children('word').map { |c| c.force_encoding(Encoding::UTF_8) }

  def self.word_processing(word)
    if block_given?
      w = yield(word)
    else
      w = word.downcase.gsub("ё", "е")
    end
    w
  end
end

# t.dict.sort_by {|k, v| v.count}.reverse[0, 10]
# t = TextProcessing::WordTokenizer.new('0a5eadd9ebf607a2d3c72c481105bf5e')
# t.tokenize {|w| w.upcase}
# t.tokenize with_regex: '\d+'
# t.dict