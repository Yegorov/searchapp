class TextProcessing::MainIndexBuilder
  INDEX_FOLDER = "#{Rails.root}/data/indexes"
  def self.build(sub_folder=nil, reg_ext='', ext='')
    current_index_folder = INDEX_FOLDER
    current_index_folder = "#{INDEX_FOLDER}/#{sub_folder}" unless sub_folder
    dir = Dir.new(current_index_folder)
    files = dir.select { |file_name| file_name =~ /\A[A-z0-9]+#{ext}\z/ }
    dir.close

    indexs = []
    files.each do |f|
      indexs << [TextProcessing::WordTokenizer.load(nil, f), f.sub("#{ext}", '')]
    end

    merge_dict = Hash.new { |hash, key| hash[key] = [] }
    indexs.each do |ind, id|
      ind.each {|k, v| merge_dict[k] << [id, v.count] }
    end
    TextProcessing::WordTokenizer.dump(sub_folder, 'index.main', merge_dict)
  end

end

# TextProcessing::MainIndexBuilder.build
# TextProcessing::MainIndexBuilder.build(reg_ext='\.top', ext='.ext')
