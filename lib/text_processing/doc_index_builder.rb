class TextProcessing::DocIndexBuilder
  DOC_FOLDER = "#{Rails.root}/data/docs"
  INDEX_FOLDER = "#{Rails.root}/data/indexes"
  class << self
    def build_all
      dir = Dir.new(DOC_FOLDER)
      files = dir.select { |file_name| file_name =~ /\A[A-z0-9]+\.txt\z/ }
                 .map { |file_name| file_name.sub('.txt', '') }
      dir.close
      files.each { |f| build(f) }
    end
    def build(id)
      doc_file = "#{DOC_FOLDER}/#{id}.txt"
      index_file = "#{INDEX_FOLDER}/#{id}"
      if File.exists?(index_file)
        return if File.mtime(index_file) > File.mtime(doc_file)
      end

      word_tokenizer = TextProcessing::WordTokenizer.new(id)
      word_tokenizer.tokenize
      index_dict = word_tokenizer.dict
      index_dict_pipe = pipeline(index_dict)
      TextProcessing::WordTokenizer.dump(nil, id, index_dict_pipe)
      TextProcessing::WordTokenizer.dump(nil, "#{id}.top",
        index_dict_pipe.max_by(20) {|k, v| v.count }
                       .map { |k, v| [k, v.count] }
      )
    end

    def pipeline(dict)
      @pipeline ||= TextProcessing::WordPipeline.new(
        Proc.new do |dict|
          # delete stop words
          d = dict.dup
          sw = TextProcessing::StopWordsReader.load.uniq
          sw.each do |word|
            d.delete(word)
          end
          d
        end,
        Proc.new do |dict|
          # stemmer
          d = Hash.new { |hash, key| hash[key] = [] }
          dict.each do |word, positions|
            stem_word = TextProcessing::PorterStemmer.stem(word)
            # if stem_word.length == 1
            #   puts word
            #   stem_word = word
            # end
            positions.each { |pos| d[stem_word] << pos } 
          end
          d
        end,
        Proc.new do |dict|
          # zip
          dict
        end,
        Proc.new do |dict|
          # sort
          sort_array = dict.sort_by do |k, v|
            k
          end
          sort_array
        end
      )
      @pipeline.pipeline(dict)
    end

  end
end

# TextProcessing::DocIndexBuilder.build('0a5eadd9ebf607a2d3c72c481105bf5e')
# TextProcessing::DocIndexBuilder.build_all