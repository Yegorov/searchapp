require "#{Rails.root}/lib/tasks/geektimes_crawler"

namespace :web do
  #
  # bin/rake web:crawl
  # bin/rake web:crawl[10]
  #
  desc "Crawl geektimes website"
  task :crawl, [:count_pages] => :environment do |t, args|
    GeektimesCrawler.new(args[:count_pages]).crawl
  end
end
