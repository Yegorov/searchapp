require 'httpclient'
require 'nokogiri'

class GeektimesCrawler
  START_URL = "https://geektimes.ru/top/monthly/"
  NEXT_PAGE_URL = "https://geektimes.ru/top/monthly/page%d/"
  COUNT_PAGES = 5

  def initialize(count_pages=nil)
    @http = HTTPClient.new
    @http.default_header = {
      'User-Agent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
    }
    @count_pages = count_pages.to_i
    @count_pages = COUNT_PAGES if @count_pages <= 0
  end

  def crawl
    puts "Start crawl [#{Time.now}]"
    body = @http.get(START_URL).body
    links = extract_articles_links(body)
    get_and_save_articles(links)
    2.upto(@count_pages || COUNT_PAGES) do |i|
      current_url = NEXT_PAGE_URL % i
      body = @http.get(current_url).body
      links = extract_articles_links(body)
      get_and_save_articles(links)
    end
    puts "Finish crawl [#{Time.now}]"
  end

  private
  def extract_articles_links(body)
    html_doc = Nokogiri::HTML(body)
    html_doc.css('article h2 a').map {|l| l.attribute('href').value }
  end

  def get_and_save_articles(links)
    links.each do |link_url|
      puts "Loading #{link_url}"
      body = @http.get(link_url).body
      article = extract_article(link_url, body)
      Doc.new(article).save
      sleep 2
    end
  end

  def extract_article(url, body)
    article = { url: url }
    html_doc = Nokogiri::HTML(body)
    article[:title] = html_doc.css('span.post__title-text').first.content.strip
    # article[:content] = html_doc.css('div.post__text-html').first.content.strip
    article[:content] = html_doc.xpath('.//div[contains(@class, "post__text-html")]//text()')
                                .map(&:text)
                                .join(" ")
    #puts article[:title]
    article
  end
end
