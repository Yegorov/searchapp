#require "#{Rails.root}/lib/tasks/geektimes_crawler"
namespace :index do
  #
  # bin/rake index:build
  #
  desc "Build indexes for docs"
  task build: :environment do
    with_time_log "index" do
      TextProcessing::DocIndexBuilder.build_all
    end

    with_time_log "main index" do
      TextProcessing::MainIndexBuilder.build
    end
  end
end

def with_time_log(name)
  puts "Start build #{name} [#{Time.now}]"
  yield if block_given?
  puts "End build #{name} [#{Time.now}]"
end
