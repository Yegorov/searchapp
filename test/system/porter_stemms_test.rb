require "application_system_test_case"

class PorterStemmsTest < ApplicationSystemTestCase
  # test "visiting the index" do
  #   visit porter_stemms_url
  #
  #   assert_selector "h1", text: "PorterStemm"
  # end
  test "porter_stemm" do
    File.open("#{Rails.root}/test/stem_test.txt", 'r:UTF-8') do |f|
      until f.eof? do
        word, expect = f.readline.chomp.split
        actual = TextProcessing::PorterStemmer.stem(word)
        assert_equal(expect, actual)
      end
    end
  end
end
