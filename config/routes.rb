Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  resources :docs
  get 'search' => 'home#search', as: :search

  namespace :admin do
    #get '/' => 'home#index'
    root to: 'home#index'
    get 'make-index' => 'home#make_index'
    get 'crawl-geektimes' => 'home#crawl_geektimes'
    get 'zip-index' => 'home#zip_index'
    get 'ranging' => 'home#ranging'
    get 'evaluation-search' => 'home#evaluation_search'
    get 'clustering-docs' => 'home#clustering_docs'
    get 'logout' => 'home#logout'
  end
end
