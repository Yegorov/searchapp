class HomeController < ApplicationController
  def index
  end
  def search
    search_engine = TextProcessing::SearchEngine.new
    @docs = search_engine.search(params[:q])

    @count = @docs.count
    @all_count = Doc.count
    @search_time = search_engine.search_time

    @docs = Kaminari.paginate_array(@docs).page(params[:page]).per(50)
    @docs.each(&:lazy_read)
  end
end
