class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  around_action :set_settings

  def set_settings
    #@setting = Setting.new
    yield
    #@setting.save
  end
end
