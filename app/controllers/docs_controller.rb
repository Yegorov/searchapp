class DocsController < ApplicationController
  http_basic_authenticate_with name: "artem", password: "admin",  only: [:create, :destroy]

  def index
    @docs = Doc.all
    @docs = Kaminari.paginate_array(@docs).page(params[:page]).per(20)
    @docs.each(&:lazy_read)
  end
  def show
    @doc = Doc.find(id: params[:id])
    @doc.try(:read)

    if params[:highlight]
      user_input = params[:q] || ""
      search_engine = TextProcessing::SearchEngine.new
      positions = search_engine.search_position(user_input, @doc[:id])

      content = @doc.content

      pos = []
      positions.each do |position|
        pos += position[1]
      end

      _START_TAG = '<b style="background: yellow">'.freeze
      _END_TAG = '</b>'.freeze
      _COUNT_START_TAG = _START_TAG.length
      _STEP_OFFSET = _COUNT_START_TAG + _END_TAG.length

      offset = 0
      pos.sort.each do |start_pos|
        content = content.insert(start_pos + offset, _START_TAG) # <b>
        end_pos = content.index(/[^а-яё]/i, start_pos + offset + _COUNT_START_TAG) # 3
        content = content.insert(end_pos || -1, _END_TAG) #</b>
        offset += _STEP_OFFSET # 7
      end

      @doc.content = content
    end

    # respond_to do |format|
    #   format.txt { render 'show.html.erb' }
    # end
  end
  def new
    @doc = Doc.new
  end
  def create
    @doc = Doc.new(**doc_params.to_hash.symbolize_keys)
    if @doc.valid?
      @doc.save
      redirect_to action: :show, id: @doc[:id]
    else
      render :new
    end
  end
  def edit
    @doc = Doc.find(id: params[:id])
    @doc.try(:read)
  end
  def destroy
    @doc = Doc.find(id: params[:id])
    @doc.try(:delete)
    redirect_to action: :index
  end

  private
  def doc_params
    params.require(:doc).permit(:title, :url, :content)
  end
end
