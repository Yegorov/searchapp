require "#{Rails.root}/lib/tasks/geektimes_crawler"

class Admin::HomeController < ApplicationController
  http_basic_authenticate_with name: "artem", password: "admin"

  def index
  end
  def make_index
    start_time = Time.now
    TextProcessing::DocIndexBuilder.build_all
    TextProcessing::MainIndexBuilder.build
    render inline: "<p>Индекс построен успешно за #{Time.now - start_time} секунд</p>"
  end
  # https://ru.wikipedia.org/wiki/TF-IDF
  # def zip_index
  #   render inline: "<p>zip_index</p>"
  # end
  # def ranging
  #   render inline: "<p>ranging</p>"
  # end
  # def evaluation_search
  #   render inline: "<p>evaluation_search</p>"
  # end
  # def clustering_docs
  #   render inline: "<p>clustering_docs</p>"
  # end
  def crawl_geektimes
    start_time = Time.now
    ::GeektimesCrawler.new(1).crawl
    render inline: "<p>Загрузка документов выполнена успешно за #{Time.now - start_time} секунд</p>"
  end
  def logout
    redirect_to :root
  end
end
