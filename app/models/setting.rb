require "yaml"

class Setting
  FILENAME = "#{Rails.root}/data/settings.yml"

  def initialize
    File.open(FILENAME, 'r:UTF-8') do |f|
      @data = YAML.load(f) || {}
    end
  end

  def added_text?
    @data[:added_text] ||= false
  end
  def added_text=(added_text)
    @data[:added_text] = added_text
  end
  def added_text!
    @data[:added_text] = true
  end

  def save
    File.open(FILENAME, 'w:UTF-8') do |f|
      f.write(@data.to_yaml)
    end
  end
end
