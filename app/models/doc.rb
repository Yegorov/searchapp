require 'digest'

class Doc
  include ActiveModel::Model

  attr_accessor :id, :url, :title, :content
  validates_presence_of :url, :content

  FOLDER = "#{Rails.root}/data/docs"

  def initialize(id:nil, url:nil, title:nil, content:nil)
    @id = id
    @url = url
    @title = title
    @content = content

    @is_lazy_readed = false
    @is_readed = false
  end

  def save
    if valid?
      @id = Digest::MD5.hexdigest(@url)
      @id += ".txt"
      File.open("#{FOLDER}/#{@id}", 'w:UTF-8') do |f|
        f.write(@url)
        f.write("\n")
        f.write(@title)
        f.write("\n\n")
        f.write(@content)
      end
    else
      #raise Exception.new("doc not valid")
    end
  end

  def read
    if @id == nil
      raise Exception.new("can't read file (id not initialize)")
    end
    if @is_readed
      return self
    end
    File.open("#{FOLDER}/#{@id}", 'r:UTF-8') do |f|
      @url = f.readline.chomp
      @title = f.readline.chomp
      f.readline
      @content = f.read
    end
    @is_readed = true
    @is_lazy_readed = true
    self
  end

  def lazy_read
    # read only title and url
    if @id == nil
      raise Exception.new("can't read file (id not initialize)")
    end
    if @is_lazy_readed
      return self
    end
    File.open("#{FOLDER}/#{@id}", 'r:UTF-8') do |f|
      @url = f.readline.chomp
      @title = f.readline.chomp
    end
    @is_lazy_readed = true
    self
  end

  def exists?
    File.exists?("#{FOLDER}/#{@id}")
  end

  def delete
    if @id == nil
      raise Exception.new("can't read file (id not initialize)")
    end
    File.delete("#{FOLDER}/#{@id}")
  end

  def [](key)
    case key
    when :id
      @id.sub('.txt', '')
    when :title
      @title
    when :content
      @content
    else
      nil
    end
  end

  def inspect
    "#<Doc @id=#{@id}>"
  end

  def self.all
    dir = Dir.new(FOLDER)
    files = dir.select { |file_name| file_name =~ /\A[A-z0-9]+\.txt\z/ }
    dir.close
    files.map { |file_name| self.new(id:file_name) }
  end
  def self.find(id:)
    id = "#{id}.txt" unless id.end_with?('.txt')
    self.all.detect { |d| d.id == id }
  end
  def self.count
    dir = Dir.new(FOLDER)
    files = dir.select { |file_name| file_name =~ /\A[A-z0-9]+\.txt\z/ }
    dir.close
    files.count
  end
end
